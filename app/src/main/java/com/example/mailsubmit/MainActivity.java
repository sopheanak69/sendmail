package com.example.mailsubmit;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements CardDialog.CardDialoCallBack {
static final String CARD_DIALOG="Card_Dialog";
private TextView textViewSubscribed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button button = findViewById(R.id.btn_Subscribe);
        textViewSubscribed = findViewById(R.id.txtDisplay);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CardDialog cardDialog = new CardDialog();
                cardDialog.show(getSupportFragmentManager(),CARD_DIALOG);
            }
        });
    }

    @Override
    public void getEmail(String msg) {
        textViewSubscribed.setText(msg);
    }
}

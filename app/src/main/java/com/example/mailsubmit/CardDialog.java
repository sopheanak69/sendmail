package com.example.mailsubmit;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class CardDialog extends DialogFragment {
    public CardDialoCallBack cardDialoCallBack;
    public interface CardDialoCallBack{
        void getEmail(String msg);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        cardDialoCallBack=(CardDialoCallBack) context;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder= new AlertDialog.Builder(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.activity_subsribe,null);
        final Button buttonSubmit = view.findViewById(R.id.btnSubmit);
        final Button buttonCancel= view.findViewById(R.id.btnCancel);
        final EditText editTextEmail = view.findViewById(R.id.txtEmail);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardDialoCallBack.getEmail(editTextEmail.getText().toString());
                Toast.makeText(getActivity(),"Subscribed",Toast.LENGTH_LONG).show();
                dismiss();
            }
        });
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(),"Cancel",Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });
        builder.setView(view);
        return builder.create();
    }
}
